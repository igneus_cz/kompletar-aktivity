# Úvod do Denní modlitby církve - aktivity (nejen) pro společenství katolické mládeže

## Licence

Knížka je volně šiřitelné dílo, podléhající podmínkám licence
[Creative Commons Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)](https://creativecommons.org/licenses/by-sa/3.0/deed.cs)
